# -*- coding: utf-8 -*-
"""
Created on Mon Jun 26 20:39:28 2017
chap7
@author: syo
"""

hello = "Hello"
print(id(hello))
print(type(hello))

str1 = "aBcdE"
print(str1.lower())
print(str1.upper())

class Human:
    age = 0#年齢
    lastname = ""
    firstname = ""
    height = 0.0 #[m]
    weight = 0.0 #[kg]

    def get_bmi(self):
        return (self.weight) / (self.height ** 2)


a = Human()
a.age = 30
a.lastname = "tanaka"
a.firstname = "ichiro"
a.height = 1.802
a.weight = 70.4

print(a.age)
print(a.lastname + " " + a.firstname)
print(a.get_bmi())

#継承
class A:
    name = "Class A"
    def hello(self):
        print("Class A says Hello")
    
class B(A):
    def hello(self):
        print("Class B says Hello")

b = B()
print(b.name)
print(b.hello())
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 25 10:51:57 2017
chap5_1
@author: syo
"""

#辞書型変数作り方
c_league = {
        "giants":"読売ジャイアンツ",
        "tigers":"阪神タイガース",
        "dragons":"中日ドラゴンズ"
        }

#dict関数を使う場合にはキーワードに引用符は不要
c_league2 = dict(
        giants = "読売ジャイアンツ",
        tigers = "阪神タイガース",
        dragons = "中日ドラゴンズ"
        )

print(c_league["dragons"])
print(c_league2["tigers"])


for d in c_league:
    print(d)
    print(c_league[d])

hash_array = dict(age =35, name = 'john')

if 'name' in hash_array:
    print("nameはあります")
    
if 'aaa' not in hash_array:
    print("aaaというキーはありません")

#キーワード可変長引数
def foo(**kargs):
    print(kargs)
    
foo(bar="bar", hoge="hoge",num=999)

d2 = dict(name="john", mail="xxx", country="US")
for key, value in d2.items():
    print(key + "->" + value)
    
#可変長引数
def foo2(*args):
    for v in args:
        print(v)
        
foo2("bar", 999, [1,3,4], (3,5,5))

#集合
a_set = set(["A","B","C","D","E","F"])
b_set = set(["A","C","F","G","H","I"])

#和集合
print(a_set | b_set)
#差集合
print(a_set - b_set)
#積集合
print(a_set & b_set)
#排他集合
print(a_set ^ b_set)
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 26 20:58:49 2017
chap7_practice
@author: syo
"""
#アドレス帳の作成

import datetime


class AddressBook:
    #アドレスに登録された人一覧
    person_list = []
    
    def add(self, person):
        #新規にアドレス帳に人を追加
        self.person_list.append(person)
        
    def show_all(self):
        #登録された個人一覧を表示
        for person in self.person_list:
            print(person.lastname + " " + person.firstname)
        
    def search(self, keyword):
        #検索条件にマッチする人を表示
        for person in self.person_list:
            if keyword in person.firstname or keyword in person.lastname:
                print(person.lastname + " " + person.firstname)
                
    def import_date(self,file):
        import csv
        import datetime
        with open(file,"r",encoding='utf-8') as f:
            reader = csv.reader(f)
            header = next(reader)#ヘッダーがあればスルー
            
            for row in reader:
                #rowに１行ごとのデータが配列として入ってくる
                p = Person()
                p.lastname = row[0]
                p.firstname = row[1]
                p.mail_address = row[2]
                #日付の文字列から日付型のデータを作る
                p.birthday = datetime.datetime.strptime(row[3],"%Y/%m/%d")
                p.tel = row[4]
                
                self.person_list.append(p)

    
class Person:
    firstname = ""
    lastname = ""
    tel = ""
    mail_address = ""
    birthday = datetime.datetime(2000,1,1)
    

#テストコード
ab = AddressBook()
ab.import_date("sample100.csv")
print("アドレスの人数->" + str(len(ab.person_list)) + "人")
ab.search("片山")


#p = Person()
#p.firstname = "michitaka"
#p.lastname = "yumoto"
#p.tel = "090-1234-5678"
#
#ab.add(p)
#
#p2 = Person()
#p2.firstname = "John"
#p2.lastname = "Lennon"
#p2.tel = "090-4321-9876"
#
#print("---動作確認---")
#ab.add(p2)
#
#print("アドレス帳の人数->" + str(len(ab.person_list)) + "人")
#
#print("---一覧表示---")
#ab.show_all()
#
#print("---検索---")
#ab.search("John")
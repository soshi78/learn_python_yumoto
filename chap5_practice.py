# -*- coding: utf-8 -*-
"""
Created on Sun Jun 25 16:15:43 2017
chap5_practice
@author: syo
"""

#practice_1
dict_var = dict(price=1000,
                name="靴下",
                stock=50,
                code="CY001")

for key, val in dict_var.items():
    print(key + "->" + str(val))
    
#practice_2
def get_sum(**kargs):
    total = 0
    start = kargs["start"]
    end = kargs["end"]
    for s in range(start, end, 1):
        total += s
    return total

print(get_sum(start=1, end=10))
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 25 09:47:29 2017

chap3

@author: syo
"""

array = [1,2,3,4,5]
print(array)

print(array[2])
#配列の後ろのほうから指定
print(array[-1])

#ループ処理
for a in array:
    if a == 4:
        break
    if a == 2:
        continue
    print("配列要素" + str(a))
# -*- coding: utf-8 -*-
"""
Created on Sun Jun 25 10:44:04 2017
chap4_random_arry
@author: syo
"""

import random

list_len = 100
rList = []

for v in range(list_len):
    rList.append(random.randint(1,100))

print(rList) 

#関数化
def make_random_array(size):
    rList = []
    for v in range(size):
        rList.append(random.randint(1,30))
    return rList

print(make_random_array(10))
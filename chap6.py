# -*- coding: utf-8 -*-
"""
Created on Sun Jun 25 16:28:55 2017
chap6
@author: syo
"""
try:
    f = open("test.txt","w")
    f.write("test")
except FileNotFoundError as ioe:
    print("file not found")
finally:    
    f.close()

try:
    file = open("test.txt","r")
    whole_str = file.read()
    print(whole_str)
    file.close()

except FileNotFoundError as fne:
    print("file not found")
    
#withステートメント
try:
    with open("text1.txt") as f:
        print(f.read())
except FileNotFoundError as fne:
    print(fne)
    
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 26 20:29:59 2017
chap6_practice
@author: syo
"""

#practice_1
def test(idx):
    try:    
        abc=["a","b","c"]
        print(abc[idx])
    except IndexError as ie:
        print("インデックスの範囲外です")
        
test(10)        
        
#practice_2
def add_number(a,b):
    try:
        return a+b
    except TypeError as te:
        print("タイプエラーが発生しました")
        
add_number(2,3)
add_number(5,"4")        
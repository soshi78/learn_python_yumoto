# -*- coding: utf-8 -*-
"""
Created on Sun Jun 25 09:11:47 2017

@author: syo
"""

"""
FizzBuzz
"""

num =  int(input("数値を入力してください:"))

if num % 3 == 0 and num % 5 == 0:
    print("FizzBuzz")
elif num % 3 == 0:
    print("Fizz")
elif num % 5 == 0:
    print("Buzz")
else:
    print(num)
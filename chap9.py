# -*- coding: utf-8 -*-
"""
Created on Sat Jul  1 22:54:49 2017
chap9
@author: syo
"""

import urllib.request
from bs4 import BeautifulSoup
req = urllib.request.urlopen("http://quality-start.in/company/")
soup = BeautifulSoup(req, "html.parser")
print(soup.find("h1").text)
contact = soup.find('a',class_='btn')
print(contact.string)

side_menu = soup.find('div',class_='list-group')
for a_tags in side_menu.find_all('a'):
    print(a_tags.string)

# -*- coding: utf-8 -*-
"""
Created on Sun Jun 25 10:27:12 2017
chap4_1
@author: syo
"""

#.pyの拡張子は不要
import platform

print(len("python"))
print(len([1,2,3]))

print(pow(2,5))

arry = [42,53,1,-43]
print(max(arry))
print(min(arry))

for v in range(1,10,2):
    print(v)
    


def add_one(num):
    return num+1
#関数定義よりも上に書くとエラーとなる
print(add_one(4))

#OSの種類を表示する
print(platform.system())
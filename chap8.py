# -*- coding: utf-8 -*-
"""
Created on Sat Jul  1 22:12:02 2017
chap8_1
@author: syo
"""

import unittest

#テスト対象のplus関数
def plus(a,b):
    return a+b

class PlusTest(unittest.TestCase):
    #テストプログラム
    def test_plus(self):
        self.assertEqual(10, plus(2,8))
        self.assertEqual(10, plus(-12,2))
        
if __name__ == "__main__":
    unittest.main()